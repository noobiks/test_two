import React, { Component } from 'react';
import {StyledJumbotron,StyledBTN, StyledA, StyledH2, StyledH4, StyledContainer, StyledCard} from '../Styled'
import {Link} from 'react-router-dom';
import { Row, Col, CardTitle, CardText, CardDeck, CardBody, Alert} from 'reactstrap';
import styled from "styled-components";

const SubtitleJB = styled.p`
    font-size: 27px;
    font-weight: 300;
    color: #fff;
    max-width: 800px;
    margin: 0 auto;
  }
`;

class LandingPage extends Component {

    render() {
        return (
            <div>
                <Alert color="success" className="mb-0 text-center">
                    <h4 className="alert-heading">Новогодняя акция проекта "Весточка" и фонда "Добра"</h4>
                    <Link to="/vestochki-dobra"><StyledBTN color="primary" size="lg">Отправь добрую весточку!</StyledBTN></Link>
                </Alert>
                <StyledJumbotron className="text-center">
                    <h1 className="display-3">Весточка</h1>
                    <SubtitleJB>
                        Сервис упрощающий отправку писем и формирование посылок в исправительные учреждения Республики Беларусь
                    </SubtitleJB>
                    <img className="img-fluid" src="/img/iphonex.png" alt=""/>
                </StyledJumbotron>
                <StyledContainer>
                    <Row>
                        <Col>
                            <StyledH2>Что такое Весточка?</StyledH2>
                            <p>
                                Весточка - сервис упрощающий отправку писем и формирование посылок в исправительные учреждения Республики Беларусь
                            </p>
                            <p>
                                Наш сервис будет взаимодействовать со всеми исправительными учреждениями
                                Республики Беларусь. Для того что бы отправить письмо своему другу или родственнику,
                                Вам достаточно будет заполнить форму на нашем сайте, написать письмо и нажать одну кнопку!
                            </p>
                            <p>
                                С нашим сервисом Вы всегда сможете узнать какая работа проводится с письмом.
                                Уведомления о статусе письма будут приходить Вам на почту. Вы всегда будете
                                проинформированы, если Ваше письмо не прошло цензуру и сможете повторить свой запрос.
                            </p>
                            <CardDeck>
                                <StyledCard>
                                    <CardBody>
                                        <CardTitle>Просто</CardTitle>
                                        <CardText>Отправка осуществляется онлайн из любой точки мира</CardText>
                                    </CardBody>
                                </StyledCard>
                                <StyledCard>
                                    <CardBody>
                                        <CardTitle>Прозрачно</CardTitle>
                                        <CardText>Вы можете контролировать статус Вашего письма или посылки.</CardText>
                                    </CardBody>
                                </StyledCard>
                                <StyledCard>
                                    <CardBody>
                                        <CardTitle>Надежно</CardTitle>
                                        <CardText>Ваше письмо и посылка будут доставлены адресату</CardText>
                                    </CardBody>
                                </StyledCard>
                            </CardDeck>
                        </Col>
                    </Row>
                </StyledContainer>
                <StyledContainer>
                    <Row>
                        <Col>
                            <StyledH2>СМИ о нас:</StyledH2>
                            <ul>
                                <li><StyledA href="https://www.the-village.me/village/city/future/265703-app">«Говорили, что нас самих посадят»: Беларусы создают важное приложение для заключенных</StyledA></li>
                                <li><StyledA href="https://dev.by/news/vestochka">«Как два айтишника убедили силовиков сделать сервис по доставке писем за решётку»</StyledA></li>
                            </ul>
                        </Col>
                    </Row>
                </StyledContainer>
                <StyledContainer>
                    <Row>
                        <Col>
                            <StyledH2>Часто задаваемые вопросы:</StyledH2>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" sm="6">
                            <StyledH4>Когда сервис заработает?</StyledH4>
                            <p>
                                На данный момент сервис проходит этап согласования в ДИНе.
                                Без одобрения ДИНа мы не можем начать внедрение проекта.
                            </p>
                        </Col>
                        <Col xs="12" sm="6">
                            <StyledH4>Как можно будет узнать статус письма?</StyledH4>
                            <p>
                                Уведомления о статусе письма будут приходить Вам на почту.
                                Вы всегда будете проинформированы, если Ваше письмо не прошло цензуру и сможете
                                повторить свой запрос.
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" sm="6">
                            <StyledH4>Это будет дорого?</StyledH4>
                            <p>
                                Нет! Стоимость отправки письма будет примерно сопостовима стоимости заказного письма
                                обычной почты. Что касается посылки, то мы будем брать небольшую комиссию от суммы
                                вашего заказа.
                            </p>
                        </Col>
                        <Col xs="12" sm="6">
                            <StyledH4>Как быстро письмо дойдет до цензора?</StyledH4>
                            <p>
                                Письмо будет доставлено в кабинет к цензору сразу после отправки. Учитывая что система
                                будет значительно упрощать работу цензора, то письма будут рассматриваться значительно
                                быстрее
                            </p>
                        </Col>
                    </Row>
                </StyledContainer>
            </div>
        )
    }
}

export default LandingPage;
