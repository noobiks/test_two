import React  from 'react';
import styled from'styled-components';

const LoadingIcon = styled.img`
  position:relative;
  width:150px;
  height:150px;
  left:50%;
  top:50%
  margin-left: -75px;
  margin-top: -25px;
`

const Loading = (props) => {
    return (
        <LoadingIcon src="img/loading.gif"/>
    )
}

export default Loading