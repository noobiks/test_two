import styled from 'styled-components';
import {Card} from 'reactstrap';

const StyledCard = styled(Card)`
    border: 0!important;
    border-radius: 3px!important;
    box-shadow: 0 5px 7px 0 rgba(0,0,0,.04)!important;
    transition: all .3s ease!important;
    
    &::before {
        content: "";
        position: absolute;
        width: 3px;
        color: #fc73b4;
        background: -moz-linear-gradient(top,#9477b4 0,#fc73b4 100%);
        background: -webkit-linear-gradient(top,#9477b4,#fc73b4);
        background: linear-gradient(180deg,#9477b4 0,#fc73b4);
        top: 0;
        bottom: 0;
        left: 0;
    }
`;


export default StyledCard