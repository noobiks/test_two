import styled from "styled-components";
import { Container} from 'reactstrap';

const StyledContainer = styled(Container)`
    margin-top:25px;
    margin-bottom:25px;
`;

export default StyledContainer