import StyledJumbotron from "./StyledJumbotron";
import StyledBTN from "./StyledBTN";
import StyledA from "./StyledA";
import StyledH2 from "./StyledH2";
import StyledH4 from "./StyledH4";
import StyledContainer from "./StyledContainer";
import StyledH1 from "./StyledH1";
import StyledCard from "./StyledCard";
import Loading from './Loading';
import StyledLogos from './StyledLogos';

export {
    StyledJumbotron,
    Loading,
    StyledBTN,
    StyledA,
    StyledH2,
    StyledH4,
    StyledContainer,
    StyledH1,
    StyledCard,
    StyledLogos
}