import styled from 'styled-components';

const StyledA = styled.a`
    color:#fc73b4;
    
    &:hover {
    color:#9477b4;
  }
`;


export default StyledA