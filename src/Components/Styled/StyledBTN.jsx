import { Button} from 'reactstrap';
import styled from 'styled-components';

const StyledBTN = styled(Button)`
    background-image: linear-gradient(122deg,#e54595,#fd378e);
    box-shadow: 0 9px 32px 0 rgba(0,0,0,.2);
    background-color: #d33693!important;
    border-color: #e03591!important;
    a {
        color:#fff!Important
    }
    
   a:hover {
    text-decoration:none;
   }
   
   :focus {
    box-shadow: 0 0 0 0.2rem rgba(108, 117, 125, 0.18);
   }
`;


export default StyledBTN