import {Jumbotron} from 'reactstrap';
import styled from 'styled-components';



const StyledJumbotron = styled(Jumbotron)`
    background-image: linear-gradient(135deg,rgba(60,8,118,.8),rgba(250,0,118,.8));
    color:#fff;
    border-radius: 0!important;
    padding-bottom:0px!important;
    margin-bottom:0px!important;
`;


export default StyledJumbotron