import React  from 'react';
import {StyledH1,StyledContainer,StyledBTN} from '../Styled'
import {Link} from "react-router-dom";

const NotDefault = (props) => {
    return (
        <div>
            <StyledContainer className={'text-center'}>
                <StyledH1>
                    Упс! Что-то пошло не так!
                </StyledH1>
                <p>К сожалению, запрашиваемая страница не найдена</p>
                <Link to="/"><StyledBTN>Вернуться на главную</StyledBTN></Link>
            </StyledContainer>
        </div>
    )
}

export default NotDefault