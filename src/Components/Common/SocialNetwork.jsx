import React, { Component } from 'react';
import { ButtonGroup} from 'reactstrap';
import styled from'styled-components';

import {
    FacebookShareCount,
    VKShareCount,
    OKShareCount,

    FacebookShareButton,
    TwitterShareButton,
    VKShareButton,
    OKShareButton,
    TelegramShareButton,
    WhatsappShareButton,
    ViberShareButton,


    FacebookIcon,
    TwitterIcon,
    VKIcon,
    OKIcon,
    TelegramIcon,
    WhatsappIcon,
    ViberIcon,
} from 'react-share';

const CursorIcon = styled.div`
  cursor:pointer;
`

class  SocialNetwork extends Component {
    render() {
        const shareUrl = 'https://vestochka.by/vestochki-dobra';
        const title = 'Отправь бесплатную весточку добра!';
        return (
            <ButtonGroup>
                <CursorIcon className='m-1'>
                    <ViberShareButton
                        url={shareUrl}
                        quote={title}>
                        <ViberIcon
                            size={32}
                            round />
                    </ViberShareButton>
                </CursorIcon>
                <CursorIcon className='m-1'>
                    <WhatsappShareButton
                        url={shareUrl}
                        quote={title}>
                        <WhatsappIcon
                            size={32}
                            round />
                    </WhatsappShareButton>
                </CursorIcon>
                <CursorIcon className='m-1'>
                    <FacebookShareButton
                        url={shareUrl}
                        quote={title}>
                        <FacebookIcon
                            size={32}
                            round />
                    </FacebookShareButton>

                    <FacebookShareCount
                        url={shareUrl}>
                        {count => count}
                    </FacebookShareCount>
                </CursorIcon>
                <CursorIcon className='m-1'>
                    <VKShareButton
                        url={shareUrl}
                        quote={title}>
                        <VKIcon
                            size={32}
                            round />
                    </VKShareButton>

                    <VKShareCount
                        url={shareUrl}>
                        {count => count}
                    </VKShareCount>
                </CursorIcon>
                <CursorIcon className='m-1'>
                    <OKShareButton
                        url={shareUrl}
                        quote={title}>
                        <OKIcon
                            size={32}
                            round />
                    </OKShareButton>

                    <OKShareCount
                        url={shareUrl}>
                        {count => count}
                    </OKShareCount>
                </CursorIcon>

                <CursorIcon className='m-1'>
                    <TwitterShareButton
                        url={shareUrl}
                        quote={title}>
                        <TwitterIcon
                            size={32}
                            round />
                    </TwitterShareButton>
                </CursorIcon>

                <CursorIcon className='m-1'>
                    <TelegramShareButton
                        url={shareUrl}
                        quote={title}>
                        <TelegramIcon
                            size={32}
                            round />
                    </TelegramShareButton>
                </CursorIcon>
            </ButtonGroup>
        )
    }
}

export default SocialNetwork