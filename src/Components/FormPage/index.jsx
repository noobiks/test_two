import React, { Component } from 'react';
import { SocialNetwork } from '../Common';
import {getCookie, setCookie} from '../../Utils/cookie.js'
import { StyledContainer, StyledBTN, Loading, StyledLogos } from '../Styled';
import { Link } from "react-scroll";
import { Form,Input, FormGroup, Label, ButtonGroup , Col, Row, Alert, ModalBody, Modal, Progress, Button, Navbar, Collapse,
    Nav, NavItem, NavbarToggler,NavLink} from 'reactstrap';
import {Helmet} from "react-helmet";
import styled from 'styled-components';

const StyledFirstDescription = styled.p`
    font-size: 40px;
    line-height: 0.95;
    font-weight: 400;
    color: #acd9d1;
    
    @media (max-width: 700px) {
        font-size:20px;
        text-align:center;
        font-weight: 500;
    }
`;

const StyledAboutTitle = styled.h1`
    font-size: 45px;
    line-height: 1.17;
    font-weight: 300;
    color: #acd9d1;
    
    @media (max-width: 700px) {
        font-size:20px;
        text-align:center;
    }
`;

const StyledAboutDescription = styled.div`
    font-size: 20px;
    line-height: 1.45;
    font-weight: 200;
`;

const StyledMiddleIconText = styled.div`
    position:relative;
    top:110px;
    h2 {
        color: #acd9d1;
    }
    @media (max-width: 900px) {
        position:static
    }
`;

const StyledH2 = styled.h2`
    color: #acd9d1;
`;

const StyledA = styled.a`
    :hover {
        text-decoration:none;
    }
`;

const StyledFirstButtonGroup = styled(ButtonGroup)`
    position:absolute!important;
    bottom: 98px;
    transform: rotate(-35deg);
`;

const StyledLinkScroll = styled(Link)`
    display: block;
    padding: .5rem 1rem;
    cursor:pointer;
`;


class FormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userSendMessage: false,
            showConfirmModal:false,
            showSendingModal:false,
            showErrorModal:false,
            typeDestination:'prison',
            typeJailAddressee:'young',
            congratulatoryText:'',
            firtsPostcardNumber: '3',
            postCardType: '1',
            isOpen: false
        };
        this._handleSubmit = this._handleSubmit.bind(this);
        this._sendMessage = this._sendMessage.bind(this);
        this._toggleModal = this._toggleModal.bind(this);
        this._confirmSending = this._confirmSending.bind(this);
        this._nextPostCards = this._nextPostCards.bind(this);
        this._prevPostCards = this._prevPostCards.bind(this);
        this.toggle = this.toggle.bind(this);

    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    componentWillMount() {
        let sendMessageStatus = getCookie('sendMessage');
        if(sendMessageStatus) {
            this.setState({userSendMessage: true})
        }
    }

    render() {
        return (
            <div>
                <Helmet>
                    <title>Отправь весточку добра!</title>
                    <meta name="description" content="Отправь открытку тем кто не может встретить новый год дома!" />
                    <meta property="og:type" content="website" />
                    <meta property="og:title" content="Отправь 'Весточку Добра'!" />
                    <meta property="og:description" content="Отправь открытку тем кто не может встретить новый год дома!" />
                    <meta property="og:image" content="/img/fblogo.jpg" />
                    <link rel="canonical" href="https://vestochka.by/vestochki-dobra" />
                </Helmet>
                <Navbar color="light" light expand="md">
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar className="justify-content-md-center">
                        <Nav navbar>
                            <NavItem>
                                <StyledLinkScroll to="about" smooth={true} offset={-70} duration= {700}>Об акции</StyledLinkScroll>
                            </NavItem>
                            <NavItem>
                                <StyledLinkScroll to="send" smooth={true} offset={-70} duration= {700}>Отправить открытку</StyledLinkScroll>
                            </NavItem>
                            <NavItem>
                                <StyledLinkScroll to="media" smooth={true} offset={-70} duration= {700}>СМИ и медиа</StyledLinkScroll>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://www.facebook.com/DOBRAfoundation" target="_blank">
                                    <i className="fab fa-lg fa-facebook-square"></i>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://www.instagram.com/vestochkadobra/" target="_blank">
                                    <i className="fab fa-lg fa-instagram"></i>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                <StyledContainer fluid>
                    <Row>
                        <Col md="6" className="text-center">
                           <div className="position-relative d-none d-md-block">
                               <img src={'/img/postcards-'+this.state.firtsPostcardNumber+'.jpg'} alt="" className="img-fluid"/>
                               <br/>
                               <StyledFirstButtonGroup className="mt-3">
                                   <StyledBTN onClick={this._nextPostCards}><i className="fas fa-arrow-circle-left"></i></StyledBTN>
                                   <StyledBTN onClick={this._prevPostCards}><i className="fas fa-arrow-circle-right"></i></StyledBTN>
                               </StyledFirstButtonGroup>
                           </div>
                        </Col>
                        <Col sm="12" md="5" className="text-center text-md-left mt-0 mt-sm-5">
                            <StyledFirstDescription className="mt-2 mt-sm-5">
                                В новогодние праздники каждый из нас хотел бы знать, что в мире есть человек,
                                который желает ему добра.
                            </StyledFirstDescription>
                            <Alert color="info" className="text-center">
                                2019 весточек добра людям, встречающим Новый год в одиночестве, собраны всего за 2 дня!<br/>
                                Спасибо большое за участие!<br/>
                                Но осталась возможность отправить открытку в места лишения свободы!
                            </Alert>
                            <Link to="send" smooth={true} offset={-70} duration= {700}>
                                <StyledBTN className="mt-3 mt-md-0 mt-md-4 btn-lg">
                                    Отправь добрую весточку
                                </StyledBTN>
                            </Link>
                        </Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <StyledLogos src="/img/our-logos.jpg" alt=""/>
                        </Col>
                    </Row>
                </StyledContainer>

                <StyledContainer id="about">
                    <Row>
                        <Col className='text-center text-md-left' >
                            <StyledAboutTitle>Что такое "Весточки добра"?</StyledAboutTitle>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <StyledAboutDescription>
                                <p>
                                    2019 весточек добра: победитель Social Weekend проект "Весточка" и фонд "Добра"
                                    предлагают отправить открытки людям, встречающим Новый год в одиночестве.
                                </p>
                            </StyledAboutDescription>

                            <StyledAboutDescription>
                                <p>
                                    Акция родилась из идеи расширить возможности сервиса "Весточки" и ускорить
                                    реализацию проекта. До этого он предлагал отправлять посылки и письма только в места
                                    лишения свободы. К новогодним праздникам появится возможность бесплатно отправить
                                    2019 открыток не только в исправительные учреждения, а еще и в интернаты для
                                    престарелых и людей с инвалидностью, ветеранам и одиноким пенсионерам, в
                                    больницы пациентам, которые встречают новогодние праздники на длительном
                                    стационарном лечении.
                                </p>
                            </StyledAboutDescription>

                            <StyledAboutDescription>
                                <p>
                                    В открытке есть возможность оставить короткое (до 200 символов) пожелание адресату.
                                    Все пожелания на открытки перенесут волонтеры от руки.
                                </p>
                            </StyledAboutDescription>

                            <StyledAboutDescription>
                                <p>
                                    Акция пройдет в два этапа. <b>Первый</b> - сбор 2019 весточек и отправка.
                                    Он начнется 24 декабря. Пожелания на открытки будут переносить друзья фонда от
                                    руки. Отправлять весточки будут партиями, первую из которых положат в почтовые
                                    ящики уже в начале января. А вот <b>второй этап</b> по сути запустит новую акцию.
                                </p>
                            </StyledAboutDescription>
                            <StyledAboutDescription>
                                <p>
                                    Каждую открытку организаторы планируют отправлять в конверте, куда положат еще один
                                    с марками и попросят в ответном послании рассказать, какое место человек хотел бы
                                    увидеть из окна в данный момент: будь это отчий дом или главная елка страны.
                                    Ответы соберут в единую базу на сайте “Весточки” в открытом доступе и предложат
                                    волонтерам сделать фотографии. Когда пожелание будет исполнено, весточка со снимком
                                    снова направится к человеку, который описал этот вид из окна в письме, но по разным
                                    причинам не может оказаться там, где хотел.
                                </p>
                            </StyledAboutDescription>
                        </Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col md="6">
                            <StyledMiddleIconText className='text-center mt-5 mt-sm-2 mt-pl-5 mt-pr-5 pl-0 pr-0'>
                                <i>
                                    <StyledH2>Как отправить весточку?</StyledH2>
                                </i>
                            </StyledMiddleIconText>
                        </Col>
                        <Col md="6" className="text-lg-right text-center d-none d-md-block">
                            <img src="/img/postcards-4.jpg" alt=""  width="300px"/>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <StyledAboutDescription>
                                <ol className="mt-md-5">
                                    <li>Выбери, кому хотел бы подарить открытку.</li>
                                    <li>
                                        Оставь пожелание до 200 символов, которое потом волонтеры перенесут от руки на
                                        обратную сторону открытки.
                                    </li>
                                    <li>
                                        Прими нашу благодарность и позволь поздравить с Новым годом и Рождеством!
                                        Будем рады, если расскажешь об акции в соцсетях.
                                        Спасибо за участие, добрый человек!
                                    </li>
                                </ol>
                            </StyledAboutDescription>

                        </Col>
                    </Row>
                </StyledContainer>

                {this.state.userSendMessage ?
                    <StyledContainer  id="send">
                        <Row>
                            <Col sm="12" md={{ size: 6, offset: 3 }} className='text-center'>
                                <StyledH2 className="mb-4">Ваша весточка добра отправлена!</StyledH2>
                                <StyledLogos src="/img/thankyou.jpg" alt="" width="150px"/>
                                <StyledH2 className="mt-4">Спасибо что приняли участие в нашей акции!</StyledH2>
                            </Col>
                            <Col className="text-center">
                                <StyledH2 className="mt-4">Расскажи о ней друзьям!</StyledH2>
                                <SocialNetwork/>
                            </Col>
                        </Row>
                    </StyledContainer>
                    :
                    <StyledContainer id="send" className="mt-md-5">
                        <Row>
                            <Col xs="12" sm="6">
                                <Form onSubmit={this._handleSubmit} className="mt-md-5">
                                    <FormGroup>
                                        <Label for="postCardType">Вид открытки:</Label>
                                        <Input type="select"
                                               name="postCardType"
                                               id="postCardType"
                                               onChange={this._handleUserInput} value={this.state.postCardType}>
                                            <option value="1">Вариант 1</option>
                                            <option value="2">Вариант 2</option>
                                            <option value="3">Вариант 3</option>
                                            <option value="4">Вариант 4</option>
                                            <option value="5">Вариант 5</option>
                                            <option value="6">Вариант 6</option>
                                        </Input>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="typeDestination">Кому отправляем</Label>
                                        <Input type="select"
                                               name="typeDestination"
                                               id="typeDestination"
                                               onChange={this._handleUserInput} value={this.state.typeDestination} disabled>
                                            <option value="prison">Людям в местах лишения свободы</option>
                                            <option value="hospital">Встречающим Новый год в больнице по состоянию здоровья</option>
                                            <option value="boarding">В интернаты, где живут пожилые и люди с инвалидностью</option>
                                            <option value="alone">Одиноким пенсионерам</option>
                                        </Input>
                                    </FormGroup>
                                    {this.state.typeDestination === 'prison'? <FormGroup>
                                        <Label for="typeJailAddressee">А конкретнее:</Label>
                                        <Input type="select"
                                               name="typeJailAddressee"
                                               id="typeJailAddressee"
                                               value={this.state.typeJailAddressee}
                                               onChange={this._handleUserInput}>
                                            <option value="young">Подросткам</option>
                                            <option value="econimic">Взрослым, осужденным за экономические преступления</option>
                                            <option value="non-economic">Взрослым, осужденным за неэкономические преступления</option>
                                        </Input>
                                    </FormGroup> : ''}
                                    <FormGroup>
                                        <Label for="congratulatoryText">Пожелание (Ограничение 200 символов)</Label>
                                        <Input type="textarea"
                                               name="congratulatoryText"
                                               id="congratulatoryText"
                                               value={this.state.congratulatoryText}
                                               required="required"
                                               onChange={this._handleUserInput} />
                                    </FormGroup>
                                    <StyledBTN className="btn-block">Отправить</StyledBTN>
                                </Form>
                                <Alert color="info">
                                    ***Если вы хотите отправить одну из открыток своему знакомому, пожалуйста,
                                    свяжитесь с командой весточки. Для этого напишите на почту vestochkaby@mail.ru
                                </Alert>
                            </Col>
                            <Col xs="12" sm="6" className="text-center">
                                <StyledH2 className="text-center mt-md-2 mt-5">Ваша открытка:</StyledH2>
                                <img src={'/img/postCard-original-'+this.state.postCardType+'.jpg'} alt="" className="img-fluid" width="300px;"/>
                            </Col>
                        </Row>
                    </StyledContainer>
                }

                <StyledContainer id="media">
                    <Row>
                        <Col md="6" className="text-lg-left text-center d-none d-md-block">
                            <img src="/img/postcards-2.jpg" alt=""  width="300px"/>
                        </Col>
                        <Col md="6">
                            <StyledMiddleIconText className='text-center mt-5 pl-5 pr-5'>
                                <i>
                                    <StyledH2>Информация для прессы</StyledH2>
                                </i>
                            </StyledMiddleIconText>
                        </Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col md={2} className=" d-none d-sm-block"></Col>
                        <Col sm={12} md={4} className="text-center">
                           <StyledA href="https://docs.google.com/document/d/1Lg3KJkwi8Up-Aa8t7-_K-LbxTMLqoVtiCWqbapibCRw/edit?usp=sharing" target="_blank" rel="noopener noreferrer" > <StyledBTN className="btn-block">Пресс-релиз</StyledBTN></StyledA>
                        </Col>
                        <Col sm={12} md={4}  className=" text-center">
                            <p>
                                Если остались вопросы, напиши
                                на globalcompactbelarus@gmail.com
                            </p>
                        </Col>
                        <Col  md={2} className="d-none d-sm-block"></Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col className="text-center">
                            <StyledH2>О нас говорят:</StyledH2>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={2} className="d-none d-sm-block"></Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://citydog.by/" target="_blank" rel="noopener noreferrer"><img src="img/company-CD.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://news.tut.by/society/620421.html" target="_blank" rel="noopener noreferrer"><img src="img/company-TB.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2}  className="text-center">
                            <a href="https://minsknews.by/vestochki-dobra-belorusam-predlagayut-otpravit-otkryitki-lyudyam-kotoryie-vstretyat-novyiy-god-v-odinochestve/" target="_blank" rel="noopener noreferrer"><img src="img/company-MN.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://www.belta.by/society/view/belorusov-priglashajut-prinjat-uchastie-v-aktsii-po-otpravke-pozdravitelnyh-otkrytok-odinokim-ljudjam-330678-2018/" target="_blank" rel="noopener noreferrer"><img src="img/company-BT.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col sm={2} className="d-none d-sm-block"></Col>
                    </Row>
                    <Row>
                        <Col sm={2} className="d-none d-sm-block"></Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://mag.relax.by/citynews/10616543-belorusy-mogut-besplatno-otpravity-vestochku-dobra-chtoby-pozdravity-odinokih-lyudej-s-novym-godom/" target="_blank" rel="noopener noreferrer"><img src="img/company-RB.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://www.sb.by/articles/nachalsya-sbor-dobrykh-novogodnikh-otkrytok-odinokim-lyudyam.html" target="_blank" rel="noopener noreferrer"><img src="img/company-SB.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2}  className="text-center">
                            <a href="http://www.ctv.by/" target="_blank" rel="noopener noreferrer"><img src="img/company-STV.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col xs={3} sm={2} className="text-center">
                            <a href="https://radiostalica.by/" target="_blank" rel="noopener noreferrer"><img src="img/company-RS.png" alt="" className="img-fluid"/></a>
                        </Col>
                        <Col sm={2} className="d-none d-sm-block"></Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col className="text-center">
                            <StyledH2>Поддержали проект:</StyledH2>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={4} md={5}  className="d-none d-sm-block"></Col>
                        <Col xs={4}  className="d-block d-sm-none"></Col>
                        <Col xs={4} sm={4} md={2} className="text-center">
                            <a href="https://idfinance.com/" target="_blank" rel="noopener noreferrer" title="IDFinance">
                                <img src="img/company-IDF.png" alt="" className="img-fluid"/>
                            </a>
                            <p>400 весточек</p>
                        </Col>
                        <Col sm={4} md={5} className="d-none d-sm-block"></Col>
                        <Col xs={4} className="d-block d-sm-none"></Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col className="text-center">
                            <StyledH2>Наши партнеры:</StyledH2>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={3} className="d-none d-sm-block"></Col>
                        <Col xs={4} sm={2} className="text-center">
                            <a href="http://deafsport.by" target="_blank" rel="noopener noreferrer" title="DeafSport Беларусь">
                                <img src="img/company-DS.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col xs={4} sm={2} className="text-center">
                            <a href="https://hoster.by" target="_blank" rel="noopener noreferrer" title="Hoster.by">
                                <img src="img/company-HB.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col xs={4} sm={2}  className="text-center">
                            <a href="https://vk.com/club171456438" target="_blank" rel="noopener noreferrer" title="Театр Тишины">
                                <img src="img/company-TT.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col sm={3} className="d-none d-sm-block"></Col>
                    </Row>
                    <Row>
                        <Col sm={3} className="d-none d-sm-block"></Col>
                        <Col xs={4} sm={2} className="text-center">
                            <a href="https://vk.com/molo.govorit" target="_blank" rel="noopener noreferrer" title='Молодечно говорит'>
                                <img src="img/company-MG.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col xs={4} sm={2}  className="text-center">
                            <a href="http://booksspace.tilda.ws/" target="_blank" rel="noopener noreferrer" title="Пространство доступных книг">
                                <img src="img/company-PK.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col xs={4} sm={2}  className="text-center">
                            <a href="https://www.instagram.com/skorohod_by/" target="_blank" rel="noopener noreferrer" title="Экскурсионный центр 'Скороход'">
                                <img src="img/company-EH.png" alt="" className="img-fluid"/>
                            </a>
                        </Col>
                        <Col sm={3} className="d-none d-sm-block"></Col>
                    </Row>
                </StyledContainer>

                <StyledContainer>
                    <Row>
                        <Col className="text-center">
                            <SocialNetwork/>
                        </Col>
                    </Row>
                </StyledContainer>

                <Modal isOpen={this.state.showConfirmModal} toggle={this._toggleModal}>
                    <ModalBody className="text-center">
                        <p>Отправить весточку добра?</p>
                        <ButtonGroup>
                            <StyledBTN color="primary" onClick={this._confirmSending}>Да, отправляем!</StyledBTN>
                            <Button color="secondary" onClick={this._toggleModal}>Пока нет.</Button>
                        </ButtonGroup>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showSendingModal}>
                    <ModalBody>
                        <Loading/>
                        <Progress animated  value="100">Весточка отправляется. Ожидайте..</Progress>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showErrorModal}>
                    <ModalBody className="text-center">
                        <h1 className="text-danger"><i className="fas fa-exclamation-triangle"></i></h1>
                        <h2>Весточка не отправлена!</h2>
                        <h3>Сервис работает с перебоями</h3>
                        <StyledBTN color="primary" onClick={this._confirmSending}>Попробовать еще раз!</StyledBTN>
                    </ModalBody>
                </Modal>
            </div>
        );
    }


    _handleUserInput = (e) => {
        var name = e.target.name;
        var value = e.target.value;

        if(name === "congratulatoryText" && value.length>200) {
            value = value.substr(0, 200)
        }

        this.setState({[name]: value});
    }

    _handleSubmit(e) {
        e.preventDefault();
        this.setState({showConfirmModal:true})
    }

    _toggleModal() {
        this.setState({
            showConfirmModal: !this.state.showConfirmModal
        });
    }

    _confirmSending(){
        this.setState({showConfirmModal:false})
        this.setState({showSendingModal:true})
        this._sendMessage()
    }

    _sendMessage() {
        let self = this;
        fetch('https://api.vestochka.by/posts/add', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"postCardType":self.state.postCardType, "typeDestination": self.state.typeDestination, "typeJailAddressee": self.state.typeDestination !== 'prison' ? "" : self.state.typeJailAddressee, "congratulatoryText": self.state.congratulatoryText})
        })
            .then(function(response) {
                if(response.ok) {
                    setCookie('sendMessage',true);
                    setTimeout(function(){ self.setState({userSendMessage: true,showSendingModal: false,showErrorModal: false, }) }, 4000);
                } else {
                    setTimeout(function(){ self.setState({showErrorModal: true,showSendingModal: false}) }, 4000);
                }
            })
    }

    _nextPostCards(e) {
        e.preventDefault();
        if(parseInt(this.state.firtsPostcardNumber) === 4) {
            this.setState({firtsPostcardNumber: 1})
        } else {
            this.setState({firtsPostcardNumber: parseInt(this.state.firtsPostcardNumber)+1})
        }
    }
    _prevPostCards(e) {
        e.preventDefault();
        if(parseInt(this.state.firtsPostcardNumber) === 1) {
            this.setState({firtsPostcardNumber: 4})
        } else {
            this.setState({firtsPostcardNumber: parseInt(this.state.firtsPostcardNumber)-1})
        }
    }
}

export default FormPage;
