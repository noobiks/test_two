import React, { Component } from 'react';
import LandingPage from './LandingPage'
import FormPage from './FormPage'
import {Route, Switch} from 'react-router-dom';
import {NotDefault} from '../Components/Common'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sendMessage: false
        };
    }

  render() {
      return (
          <Switch>

              <Route exact path='/' render={(props)=><LandingPage sendMessage={this.state.sendMessage}/> }  />
              <Route path="/vestochki-dobra" render={(props)=><FormPage/> } />
              <Route component={NotDefault} />

          </Switch>
    );
  }
}

export default App;
